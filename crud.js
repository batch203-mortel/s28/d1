// CRUD Operations
/*
	C - Create (Insert Document)
	R - Read/Retrieve (View Specific Document)
	U - Update (Edit specific document)
	D - Delete (Remove specific document)

	- CRUD Operations aare the heart of any backend application.
*/

// [Section] Insert a document/s (Create)

/*
	-Syntax:
	 - db.collectionName.insertOne({object});
	Comparison with javascript
	 object.object.method({object});

*/

db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact: {
		phone: "87654321"
		email: "janedoe@gmail.com"
	},
	courses: ["CSS", "JavaScript", "Python"],
	department: "none"
});



db.rooms.insertOne({
	name: "single",
    accommodates: 2,
    price: 1000,
    description: "A simple room with basic necessities",
    rooms_available: 10,
    isAvailable: false
})

db.users.insertMany([
	{
		firstName: "Stephen",
		lastName: "Hawkings",
		age: 76,
		contact: {
			phone: "87654321"
			email: "stephenhawking.com"
	},
		courses: ["Phython", "React", "PHP"],
		department: "none"
	}
	{
		firstName: "Neil",
		lastName: "Amstrong",
		age: 76,
		contact: {
			phone: "87654321"
			email: "neilamstrong.com"
	},
		courses: ["Phython", "Laravel", "SASS"],
		department: "none"
	}
	

	])

db.rooms.insertMany([
		{
			name: "double",
		    accommodates: 3,
		    price: 2000,
		    description: "A room fit for a small family going on a vacation",
		    rooms_available: 5,
		    isAvailable: false
		}
		{
			name: "queen",
		    accommodates: 4,
		    price: 4000,
		    description: "A room with a queen sized bed perfect for a simple getaway",
		    rooms_available: 15,
		    isAvailable: false
		}

	])

// [Section] retrieve/read a document(read)

	/*
		-Syntax:
			-db.collectionName.find({});
			-db.collectionName.find({field:value});
	*/

	db.users.find({});

	db.users.find({firstName: "Stephen"});

	// Find documents with multiple parameters,
		/*
			Syntax: 
				-db.collectionName.find({fieldA:valueA, fieldB: valueB})

		*/

		db.users.find({lastName: "Amstrong", age:82})

		// [Section] Udating documents (Update)

		 // Create a document to update

		 db.users.insertOne({
		 	firstName: "Test",
		 	lastName: "Test",
		 	age: 0,
		 	contact: {
		 		phone: "00000000",
		 		email: "test@gmail.com"
		 	},
		 	course:[],
		 	department: "none"
		 }) ;


		 /*
			syntax:
				- db.collectionName.updateOne({criteria}, {$set: {field: value}})

		 */

		 db.users.updateOne(
			{firstName: "Test"},
			{
				$set:{
					firstName: "Bill",
					lastName: "Gates",
					age: 65,
					contact: {
						phone: "12345678",
						email: "bill@gmail.com"
					},
					course: ["PHP", "Laravel", "HTML"],
					department: "Operations",
					status: "active"

				}
			}
		 	)

// Updating multiple documents
/*
	Syntax:
		db.collectionName.updateMany({criteria}, {$set:{value}});
*/

	db.users.updateMany(
	{department: "none"},
	{
		$set:{department: "HR"}
	}
		)

// Replace One

		/*
			Syntax:
				db.collectionName.replaceOne({criteria}, {$set:{field:value}})
		*/
	
		db.users.replaceone(
		{firstName: "Bill"},
		{firstName: "Bill",
		lastName: "Gates",
		age: 65,
		contact: {
		phone: "12345678",
		email: "bill@gmail.com"
					},
		courses: ["PHP", "Laravel", "HTML"],
		department: "Operations"}
		)


		/*
    Mini Activity:

        1. Using the hotel database, update the queen room and set the available rooms to zero.

        2. Use the find query to validate if the room is successfully updated.

        3. Take a screenshot of the Robo3t result and send it to the batch hangouts.

*/

db.rooms.updateOne(
		{name: "queen"},
		$set:{
			rooms_available: 0
		});

// Sectio: removing documents [Delete]

	/*
		db.collectionName.deleteOne({criteria});// delete the firs document found.
	*/

		db.users.deleteOne({firstName: "Test"});

// Deletemany

	/*
		-be careful when using "deleteMany" method. If no search criteria is provided, it will delete all the documents  in the collection.
		Syntax:
			- db.collectionName.deleteMany({criteria});

	*/

	db.users.deletMany({firstName: "Test"})

	// Check if the 0 available room is deleted
	db.rooms.find()

	// [SECTION] Advance queries
	/*
		
	*/

	// query an embedded document

	db.users.find({
		contact:{phone: "87654321"
				email: "stephenhawking@gmail.com"}
	})

	db.users.find({
		{contact.email: "stephenhawking.com"}
	})

	//querying an array with exat element

	db.users.find(
	{
		courses: ["CSS", "JavaScript", "Phython"]
	})

	// querying an array disregrading the array elements order.
	db.users.find(
	{
		courses: {$all: [ "JavaScript","CSS", "Phython"] }
	})

	//Querying an Embedded Array


	db.users.insertOne({
		nameArr: [
		{
			nameA: "Juan"
		},
		{
			nameB: "Tamad"
		}]
	})